import os
import sys
import json

# ENERGY3 MODULES
import interact_influxdb as iDB
import tools as tl
import import_configs as configs

''' 
PURPOSE

Allow for extraction of data and associated flags (manual / automatic / none) from measurement for the time specified

EXAMPLE CALLS (python3 extract_data.py CUSTOMER_SITE manual/auto start_date end_date)
$ python3 extract_data.py TEST_BB2 m 2020-04-01 2020-04-31 
$ python3 extract_data.py TEST_BB2 a 2020-03-01 2020-03-31 

'''

# DATABASE AND FOLDER INFORMATION
with open(os.getcwd() + "/system_information.json") as config_file:
    config = json.load(config_file)

host = config['host']
port = config['port']
user = config['user']
password = config['password']
site_config_folders = config['site_config_folders']
save_folder = config['export_folder']


# ARGUMENTS
# extract site and customer from first argument
site_customer = [sys.argv[1]]
customer, measurement = site_customer[0].split('_')
database = customer

# extract whether manual or auto flags wanted (or none) 
auto_or_man = sys.argv[2]

# extract time wanted
time = []
time_start = sys.argv[3] + ' 00:00:00+00:00'
time_end = sys.argv[4] + ' 23:50:00+00:00'
time.append(time_start.replace(' ', 'T'))  # add T for database communications
time.append(time_end.replace(' ', 'T'))  # add T for database communications


# SITE CONFIG INFORMATION (UPDATE)
s_i = configs.fetch_config_data(site_config_folders, database)

# GET SENSOR DETAILS in period of data selected (UPDATE)
one_end = ((s_i.start_dt <= str(time_start)) & (str(time_start) <= s_i.end_dt))
two_end = ((str(time_start) <= s_i.start_dt) & (s_i.start_dt <= str(time_end)))
ans = s_i[(s_i.site == measurement) & (one_end | two_end)]
sensors = ans[['name', 'type', 'prim']]
sensors = sensors.drop_duplicates()

#print(s_i)
#print()

try:

    print()
    print('===========================================================================')
    print(customer + ': ' + measurement)
    print('===========================================================================')
    print()

    print()
    print('----------------------------------------------------------------------------------------')
    print('>> 1. Extracting data and flags')   
    print('----------------------------------------------------------------------------------------')
    print()

    # EXTRACT DATA FROM MEASUREMENT
    print('        ........ data')
    what = 'value_adj, sensorname, type'
    where = ' where '
    when = ' time >=\'' + time[0] + '\' and time <=\'' + time[1] + '\''
    flags = ''
    c = iDB.influx_connection(host, port, user, password, database)
    if c.make_connection():
            query_results = c.query_database(what, measurement, flags, where, when)
            data = query_results['data']

    # DETERMINE COLUMN ORDER FROM CONFIG FILE
    columns = [c for c in data.columns]
    column_order = []
    for i in sensors.index:
        t003 = sensors.loc[i]
        newlist = [x for x in columns if x.startswith(t003['name'])]
        for n in newlist:
            column_order.append(n)

    # REORDER DATAFRAME ACCORDING TO NEEDED COLUMN ORDER
    data = data[column_order]
    data.index.name = 'TimeStamp'

    # REPLACE SENSOR NAMES FOR MAIN COLUMNS
    for i in sensors.index:
        p = sensors.loc[i]
        replace = p['name'] + '_' + p['type']
        data.rename(columns={replace: p['name']}, inplace=True)

    print()
    print()    

    # QUERY DATABASE FOR FLAGS IF MANUAL OR AUTO FLAGS SPECIFIED
    print('        ........ flags')
    type_export = 'none'
    if auto_or_man != 'n':

        # EXTRACT FLAGS FROM MEASUREMENT
        what = 'flag_manual, flag_auto, sensorname'
        where = ' where '
        when = ' time >=\'' + time[0] + '\' and time <=\'' + time[1] + '\''
        flags = '_flags'
        c = iDB.influx_connection(host, port, user, password, database)
        if c.make_connection():
            query_results = c.query_database(what, measurement, flags, where, when)
            df_manual_flags, df_auto_flags = query_results['data']


        print()
        print('----------------------------------------------------------------------------------------')
        print('>> 2. Checking that flagging is complete')   
        print('----------------------------------------------------------------------------------------')
        print()


        # JOIN DATA AND FLAGS
        if auto_or_man == 'a':
            type_export = 'auto'
            exported_data = data.join(df_auto_flags)
            print('         ........ AUTO')
            print(str(df_auto_flags.min()))

        elif auto_or_man == 'm':
            type_export = 'manual'
            exported_data = data.join(df_manual_flags)
            print('         ........ MANUAL')
            print(str(df_manual_flags.min()))
        print()
    else:
        exported_data = data

    print()
    print('----------------------------------------------------------------------------------------')
    print('>> 3. Resampling data to pad missing data')   
    print('----------------------------------------------------------------------------------------')
    print()

    # CHECK FOR MISSING TIMESTAMPS AND PAD IF ANY
    exported_data = tl.pad_missing_timestamps(exported_data, [time_start, time_end])



    # WRITE DATA TO A CSV
    filename = customer + '_' + measurement + '_' + str(exported_data.index[0].strftime('%Y%m%d')) + '_' \
        + str(exported_data.index[-1].strftime('%Y%m%d')) + '_' + type_export
    save_name = save_folder  + filename + '.csv'

    print()
    print('----------------------------------------------------------------------------------------')
    print('>> 4. Writing data to csv: ' + save_name)   
    print('----------------------------------------------------------------------------------------')
    print()
    print('         ' + save_name)
    print()
    print()

    exported_data.to_csv(save_name, index_label="TimeStamp", na_rep="nan", date_format='%d/%m/%Y %H:%M:%S',
                         columns=exported_data.columns)

except Exception as e:
    print(str(e))
