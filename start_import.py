import os
import json
import sys


'''
PURPOSE 

Facilitates the importing data into influxdb. 

1. opens the system information file for information about the folders and databases in use
2. opens the site configs folder and builds a list of sites to import
3. checks for any arguments given when the script is called, divides these into the customer_sitename and whether to 
    overwrite existing data or append data to the end. If no arguments given it will import all sites and append 
    the data
4. for each site calls 'import_data_from_files' to import the data 

EXAMPLE CALLS
$ python3 start_import.py TEST_BB2 append
$ python3 start_import.py TEST_BB2 overwrite 2020-04-01 2020-04-17

'''

# ENERGY3 MODULES
import import_data_from_files as data_importer

# DATABASE AND FOLDER INFORMATION
with open("/home/bob/working_dir/python_scripts/data_gatherer_redux/system_information.json") as config_file:
    config_sys = json.load(config_file)

# Extract site config folder
site_config_folders = config_sys['site_config_folders']
site_config_folder = site_config_folders[0]  # UPDATE - don't like non specific key   

# CHECK IF ARGUMENTS GIVEN
site = ''
overwrite_data = 0
date_range = []
# If so extract customer and site name - argument 1
if len(sys.argv) > 1:
    site = sys.argv[1]
    overwrite_data = 0
    # and whether data is to be overwritten or appended
    if sys.argv[2] == 'overwrite':
        overwrite_data = 1
        date_range.append(sys.argv[3]) # dates used to patch/overwrite data in database
        date_range.append(sys.argv[4]) 

# IMPORT DATA AND INJECT INTO INFLUXDB
# Extract names of configs in configs folder
config_list_temp = os.listdir(site_config_folder) 
config_list = []
for conf in config_list_temp:
    if conf.startswith('site_config_' + str(site)):
        config_list.append(conf)



# for each config import data
for config_list_file in config_list:
    try:
        with open(site_config_folder + config_list_file) as config_file:
            config_site = json.load(config_file)
        data_importer.go_import_data(config_site, config_sys, overwrite_data, date_range)

    except Exception as e:
        print(str(e))
















