import os
import pandas as pd
import csv
import numpy as np


def import_data(folder,filename):    
    """
    1. opens Campbell Scientific structured data files
    2. extracts data
    3. reassigns timestamps to index
    4. replaces 'NAN's with np.nan
    5. removes 'RECORD' column
    6. returns data frame
    """
    

    # READ IN DATA LINE BY LINE AND FIND LOCATION OF HEADER FILE (DENOTED BY 'TIMESTAMP')
    c001 = 0
    with open(folder + filename, 'r', encoding = "ISO-8859-1") as f:
        reader = csv.reader(f)
        for row in reader:
            if 'TIMESTAMP' in row:
                header  = row
                header_line_number = c001
            
            c001 += 1

    # READ DATA INTO PANDAS ARRAY USING FOUND HEADER LINE NUMBER
    imported_data = pd.read_csv(folder + filename, header = header_line_number + 2, sep=',', encoding = "ISO-8859-1")
    imported_data.columns = header

    # REASSIGN INDEX TO 'TIMESTAMP' AND REMOVE COLUMN NAME
    imported_data.index = pd.DatetimeIndex(imported_data['TIMESTAMP'])
    del imported_data['TIMESTAMP'] 
    imported_data.index.name = 'Timestamp'

    # REPLACE ANY INSTANCES OF "NAN"
    imported_data = imported_data.replace('NAN',np.nan )

    # DROP RECORD COLUMN - not needed
    del imported_data['RECORD']

    # RETURN DATA
    return imported_data
