import os
import pandas as pd
import numpy as np


def import_data(folder, filename):
    '''
    1. opens EOL ZENITH exported files 
    2. extracts data
    3. replaces '-'s in data with 'nan's
    4. composes new timestamp from 'Hour' and 'Date' columns
    5. assigns new timestamps to index 
    6. returns dataframe
    '''
       

    # IMPORT DATA
    full_file_name = folder + '/' + filename
    imported_data = pd.read_csv(full_file_name, header = 3,
        delim_whitespace=True, encoding = "ISO-8859-1", parse_dates=False)

    # REPLACE '-'s in data with 'nan's 
    imported_data = imported_data.replace('-', np.nan)

    
    # FORMAT DATES
    # grab hours
    hours = []
    for h in imported_data['Time']:
        hours.append(h)

    # grab dates
    dates=[]
    for d in imported_data['Date']:
        dates.append(d)

    # strip components and convert into timestamp
    times = []
    for c001 in range(0,len(dates)):        

        # split hours
        hour = str(hours[c001])
        H, m = hour.split(':')

        # split date
        date = str(dates[c001])
        d, M, Y = date.split('/')

        # if day month year round the opposite way
        if int(d) > int(Y):
            Y, M, d = date.split('/')  

        # convert timestamp to pandas thingy and append to times array
        a =  pd.Timestamp(int(Y), int(M), int(d), int(H), int(m))
        times.append(a)

    # Create pandas series from new timestamps and add to dataframe replacing index and delete 'Hour' and 'Date' columns
    imported_data.index = pd.Series(times)
    del imported_data['Time']
    del imported_data['Date'] 
    imported_data.index.name = 'TimeStamp'

    # RETURN DATA
    return imported_data
    