import unittest
import query_influxdb as qb


class Test_query_influxdb(unittest.TestCase):

    # Test - connection to database can be made
    def test_connection_made_returns_true(self,):
        c = qb.influx_connection('10.1.1.29', '8086', '', '', 'GPG')
        self.assertTrue(c.make_connection())

    # Test - database and measurement in database
    def test_database_and_measurement(self,):
        c = qb.influx_connection('10.1.1.29', '8086', '', '', 'GPG')
        self.assertEqual(c.valid_customer_and_site('BB5'), "Database, Measurement")

    # Test - measurement not in database
    def test_database(self,):
        c = qb.influx_connection('10.1.1.29', '8086', '', '', 'GPG')
        self.assertEqual(c.valid_customer_and_site('BB6'), "Database")

    # Test - database not found
    def test_database_not_found(self,):
        c = qb.influx_connection('10.1.1.29', '8086', '', '', 'Monkey')
        self.assertEqual(c.valid_customer_and_site('BB5'), "")


if __name__ == '__main__':
    unittest.main()
