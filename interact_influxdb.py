import pandas as pd
from influxdb import DataFrameClient, InfluxDBClient
import numpy as np


class influx_connection:

    def __init__(self, host, port, user, password, database):
        """
        initialise the instance with the details of the influxdb connection
        :param host: IP address
        :param port: port number of influxdb
        :param user: influxdb username
        :param password: influxdb password
        :param database: name of the client
        """
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.database = database

    def valid_customer_and_site(self, measurement):
        """
        checks that the customer and measurement exists within the database
        """

        # CHECKS DATABASE IS IN INFLUXDB
        self.client_influxDB = InfluxDBClient(host=self.host, port=self.port)
        database_list = self.client_influxDB.get_list_database()
        databases = [d['name'] for d in database_list]

        which_there = ''

        # IF DATABASE IN INFLUXDB
        if self.database in databases:

            # switch database
            self.client_influxDB.switch_database(self.database)

            # CHECK IF MEASUREMENT FOUND
            measurement_list = self.client_influxDB.get_list_measurements()
            measurements = [m['name'] for m in measurement_list]

            if measurement in measurements and measurement + '_flags' in measurements:
                which_there = 'Database, Measurement'
            else:
                which_there = 'Database'
        else:
            which_there = ''

        return which_there

    def make_connection(self):
        """ 
        make connection to the influx database
        """
        try:
            self.client = DataFrameClient(host=self.host,
                                          port=self.port,
                                          username=self.user,
                                          password=self.password,
                                          database=self.database)
            return True

        except Exception as e:
            print('No connection could be made')
            print(str(e))
            return False

    def make_connection_inject(self):
        """ 
        make connection to the influx database to inject data from json 
        """
        try:
            self.client_inject = InfluxDBClient(host=self.host,
                                          port=self.port,
                                          username=self.user,
                                          password=self.password,
                                          database=self.database)
            return True

        except Exception as e:
            print('No connection could be made')
            print(str(e))
            return False

    @staticmethod
    def __change_query_to_dataframe(df, is_flag_data):
        """
        change form of data from query result to pandas dataframe, returns different data
        depending if you need data or flags
        """

        # RENAME DATAFRAME COLUMNS - if flag data use sensorname; if data use cat of sensorname and type
        if "_flags" in is_flag_data:
            df['dataNames'] = df.sensorname
        else:
            df['dataNames'] = df.sensorname + '*' + df.type

        # EXTRACT DATA - for each of the data columns and append to list a list
        t002 = []
        t003 = []
        data_cols = df.dataNames.unique()
        for t001 in data_cols:
            if "_flags" in is_flag_data:
                s = t001
                t002.append(df.flag_auto[(df.sensorname == s)])
                t003.append(df.flag_manual[(df.sensorname == s)])
            else:
                s, t = t001.split('*')
                t002.append(df.value_adj[(df.sensorname == s) & (df.type == t)])

        # CONCATENATE DATA IN LIST(S) INTO PANDAS DATAFRAME
        if "_flags" in is_flag_data:
            data_cols = [s + '_flagged' for s in data_cols]

            flag_auto = pd.concat(t002, axis=1)
            flag_auto.columns = data_cols

            flag_manual = pd.concat(t003, axis=1)
            flag_manual.columns = data_cols
            return flag_manual, flag_auto
        else:
            data_cols = [s.replace('*', '_') for s in data_cols]
            data_ret = pd.concat(t002, axis=1)
            data_ret.columns = data_cols
            return data_ret

    def query_database(self, what, measurement, flags, where, when):
        """
        query database using provided parameters, return pandas data array
        """

        if 'Database' in self.valid_customer_and_site(measurement) and 'Measurement' \
                in self.valid_customer_and_site(measurement):
            print('         DATABASE AND MEASUREMENT FOUND')

            try:
                # QUERY DATABASE
                query_str = "SELECT " + what + " FROM " + measurement + flags + where + when
                print('         QUERY: ' + query_str)
                result_query = self.client.query(query_str)
                result_keys = result_query.keys()

                # EXTRACT DATA FROM QUERY INTO PANDAS ARRAY
                keys = [k for k in result_keys]
                df = result_query[keys[0]]

                # RETURN DATA IN NEEDED FORMAT
                return {'data': self.__change_query_to_dataframe(df, flags), 'result':self.valid_customer_and_site(measurement)}

            except Exception as e:
                print('         ........ data could not be extracted from database')
                print('         ........ ERROR MESSAGE: ' + str(e))
                
                # RETURN DATA IN NEEDED FORMAT
                return {'data': '', 'result': 'Extraction Error'}
                
        elif 'Database' in self.valid_customer_and_site(measurement) and not 'Measurement' \
                in self.valid_customer_and_site(measurement):            
            print('         DATABASE FOUND BUT NOT MEASUREMENT, CREATING MEASUREMENT')
            return {'data': '', 'result': self.valid_customer_and_site(measurement)}

        elif not 'Database' in self.valid_customer_and_site(measurement):
            print('         DATABASE NOT FOUND, CREATE DATABASE')
            return {'data': '', 'result': self.valid_customer_and_site(measurement)}
            
        else:
            print('         DATABASE AND MEASUREMENT NOT FOUND, CREATE DATABASE')
            return {'data': '', 'result': self.valid_customer_and_site(measurement)}

    def inject_data(self, info, data):
        """
        inject data into database
        """
       
        # CREATE CLIENT TO INJECT DATA
        can_inject = self.make_connection_inject()

        # IF CREATED
        if can_inject:
            
            # RUN THROUGH DATA
            dates = []
            for i in data.index:

                value_raw = data.loc[i][info['colname'] + '_raw']
                value_adj = data.loc[i][info['colname'] + '_adj']

                json_body_data = [
                {
                    "measurement": info['site'],
                    "tags": {
                        "sensorname"  : info['name'],
                        "type"        : info['type'],
                    },
                    "time": i,
                    "fields": {
                        "value_raw"       :  value_raw,
                        "value_adj"       :  value_adj,                        
                    }
                }
                ]

                # INSERT DATA INTO DATABASE - TODO - fix this logic
                try:
                    result_insert_data = self.client_inject.write_points(json_body_data)

                except Exception as e:
                    print(str(e))
                    dates.append(i)

            # CHECK HOW MUCH DATA FLAGS MADE IT INTO DATABASE, PRINT THOSE THAT DIDN'T
            # if all data injected into database
            if len(dates) == 0:
                return_string = '         DATA INJECTED INTO DATABASE'

            # if no data injected into database
            elif len(dates) == len(data.index):
                return_string = '         NO DATA INJECTED INTO DATABASE'

            # if some data not injected into database
            else:
                for u in dates:
                    print('                  ' + str(u))
                return_string = '         PARTIAL INJECTION OF DATA INTO DATABASE' 
                    
        return return_string

    def inject_flags(self, info, data):
        """
        inject flags into database
        """

        # CREATE CLIENT TO INJECT FLAGS
        can_inject = self.make_connection_inject()

        # IF CREATED
        if can_inject:
            
            # RUN THROUGH FLAGS            
            dates = []
            for i in data.index:

                flag_manual = data.loc[i][info['colname'] + '_manual']
                flag_auto = data.loc[i][info['colname'] + '_auto']

                json_body_flag = [
                {
                        "measurement": info['site'] + '_flags',
                        "tags": {
                            "sensorname"  : info['name']
                        },
                        "time": i,
                        "fields": {
                            "flag_auto"       : int(flag_auto),
                            "flag_manual"     : int(flag_manual)
                        }
                }
                ]                   
 
                # INSERT FLAGS INTO DATABASE - TODO - fix this logic
                try:
                    result_insert_data = self.client_inject.write_points(json_body_flag)

                except Exception as e:                    
                    print(str(e)) 
                    dates.append(i)

            # CHECK HOW MANY FLAGS MADE IT INTO DATABASE, PRINT THOSE THAT DIDN'T
            # if all data injected into database
            if len(dates) == 0:
                return_string = '         FLAGS INJECTED INTO DATABASE'

            # if no data injected into database
            elif len(dates) == len(data.index):
                return_string = '         NO FLAGS INJECTED INTO DATABASE'

            # if some data not injected into database
            else:
                for u in dates:
                    print('                  ' + str(u))
                return_string = '         PARTIAL INJECTION OF FLAGS INTO DATABASE' 

        return return_string
















