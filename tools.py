    
import pandas as pd
import time
from datetime import datetime, timedelta
import os
import numpy as np


def pad_missing_timestamps(imported_data, dates):
    '''
    Checks for missing timestamps .......
        1. creates ideal timestamp index from dates passed to function
        2. compares passed data timestamp index with ideal timestamp index
        3. returns padded dataframe if missing timestamps found
    '''

    # CREATE IDEAL TIME SERIES USING DATE RANGE PASSED TO FUNCTION, AND FREQUENCY DERIVED FROM DATA
    this_freq = str(pd.infer_freq(imported_data.index))
    print('         from ' + str(dates[0]) + ' to ' + str(dates[1]) + ' at frequency: ' + this_freq)

    # if cannot infer frequency from table get input
    if this_freq == "None":
        print()
        this_freq = input('Cannot infer frequency from data...... enter frequency: ')

    data_timestamp_ideal = pd.DataFrame(index = pd.date_range(dates[0], dates[1], freq=this_freq), dtype= float)

    imported_data['missing_data'] = 0   
    
    # COMPARE TIME INDICES BETWEEN IMPORTED DATA AND IDEAL DATA
    index_ideal = data_timestamp_ideal.index
    index_imported = imported_data.index

    # if not all timestamps in index
    if len(index_ideal) > len(index_imported):
        print('         ........ data missing, padding')

        # see where timestamps are missing
        index_missing = index_ideal.difference(index_imported)

        # grab first line (make values = nan, _flagged = 3 and missing_data = 1)
        # use this line to plug gaps 
        line = imported_data.iloc[0] * np.nan    
        for c in imported_data.columns:
            if '_flagged' in c:
                line[c] = 3

            if 'missing_data' in c:
                line[c] = 1  
        
        # run through missing timestamps and pad
        for m in index_missing:
            imported_data.loc[m] = line

    # RETURN PADDED, SORTED DATA
    return imported_data.sort_index()




def check_duplicate_timestamps(imported_data, dates):
    '''
    Checks for duplicate data ..........
        1. Checks for duplicate timestamps, ie timestamps match but data in rows does not, keeps last timestamp
    '''

    # CHECK FOR DUPLICATE TIMESTAMPS
    duplicate_timestamps = imported_data[imported_data.index.duplicated()]
    #print(duplicate_timestamps)
    if len(duplicate_timestamps) > 0:
        print('........ duplicate timestamps found')
        for duplicate in duplicate_timestamps.index:
            print('         ........ ' + str(duplicate))
        
        print('         ........ removing')
        imported_data = imported_data[~imported_data.index.duplicated(keep='last')]
    
    else:
        print('........ no duplicate timestamps found')    
   
    
    # RETURN PADDED DATA
    return imported_data

 

