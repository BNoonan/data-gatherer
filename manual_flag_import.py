
# Standar modules
import numpy as np
import pandas as pd
import json
import os
import shutil
from datetime import datetime, timedelta


# Energy3 modules
import query_influxdb as iDB
import tools as tl
import import_configs as configs



''' 

>> BLURB
    Imports manual flags from csv file and writes these into the database. So as to not overwrite data, 
wutoflag data are downloaded from the database and then the new manual flags and auto flags are
uploaded together.


>> HISTORY
    20190110 - script conceived
    20190401 - removed hardcoded database - database specified in filename

'''

# DATABASE AND FOLDER INFORMATION
with open(os.getcwd() + "/system_information.json") as config_file:
    config_sys = json.load(config_file)


# Extract folder information
folder = config_sys['manual_flags_folder']
folder_processing = folder +'/processing/'
folder_processed = folder + '/processed/'


# Extract database config
db_info = [config_sys['host'],
           config_sys['port'],
           config_sys['user'],
           config_sys['password']]


# SEE WHAT FILES FILES THERE ARE TO IMPORT 
files_to_import = os.listdir(folder)
for f in files_to_import:
    file_processed = 0

    p = f.split('_')
    customer = p[0] 
    site_name =p[1]

    # START IMPORTING DATA FROM DATA DIRECTORY
    print()
    print('===========================================================================')
    print(customer + ': ' + site_name)
    print('===========================================================================')
    print()


    # MOVE FILE TO PROCESSING DIRECTORY
    shutil.move(folder + f, folder_processing + f)

    # IMPORT DATA FROM MANUAL FLAG CSV INTO PANDAS DATAFRAME
    try:

        print()
        print('----------------------------------------------------------------------------------------')
        print('>> 1. Importing flags from ' + folder + f)   
        print('----------------------------------------------------------------------------------------')   

        try:     
            dateparse = lambda x: pd.datetime.strptime(x, '%d/%m/%Y %H:%M:%S')
            df_new_manual_flags = pd.read_csv(folder_processing + f, header = 0, sep=',', parse_dates=['TimeStamp'], 
                date_parser=dateparse, engine="python")
        except:
            dateparse = lambda x: pd.datetime.strptime(x, '%d/%m/%Y %H:%M')
            df_new_manual_flags = pd.read_csv(folder_processing + f, header = 0, sep=',', parse_dates=['TimeStamp'], 
                date_parser=dateparse, engine="python")
        
        # equate df index to data's index 
        df_new_manual_flags.index = pd.DatetimeIndex(df_new_manual_flags['TimeStamp'])
        df_new_manual_flags.index.name = 'Timestamp'

        start_time = str(df_new_manual_flags.index[0]) # used to pad autoflag data
        end_time = str(df_new_manual_flags.index[-1])  # used to pad autoflag data       

        file_processed += 1     

    except Exception as e:
                print(str(e))


    # IMPORT EXISTING FLAGS FOR THE PERIOD FROM DATABASE (need to upload autoflags with manual ones)
    try:
        print()
        print('----------------------------------------------------------------------------------------')
        print('>> 2. Querying database for autoflags ') 
        print('----------------------------------------------------------------------------------------')   

        time = []
        time.append(start_time.replace(' ','T') + '+00:00')
        time.append(end_time.replace(' ','T') + '+00:00')

        # EXTRACT FLAGS FROM MEASUREMENT
        what = 'flag_manual, flag_auto, sensorname'
        where = ' where '
        when = ' time >=\'' + time[0] + '\' and time <=\'' + time[1] + '\''
        flags = '_flags'

        c = iDB.influx_connection(db_info[0], db_info[1], db_info[2], db_info[3], customer)
        if c.make_connection():
            query_results = c.query_database(what, site_name, flags, where, when)
            df_manual_flags, df_auto_flags = query_results['data']

        print()
        print('Checking that flagging is complete', end='')
        print(str(df_auto_flags.min()))

        # REMOVE TIMEZONE FROM TIMESTAMP
        auto_index = df_auto_flags.index        
        auto_index = auto_index.tz_localize(None)
        df_auto_flags.index = auto_index 
        
        # PAD TIMESTAMPS IN DATABASE SOURCED DATA TO MATCH MANUAL DATA
        df_auto_flags_pad = tl.pad_missing_timestamps(df_auto_flags,[start_time,end_time])
        #df_auto_flags_pad.index = df_new_manual_flags.index # remove time zone so dataframes can be merged 

        file_processed += 1              

    except Exception as e:
                print(str(e))

    
    exit()

    try:
        print('inserting flags into database')
        # INSERT MANUAL FLAGS BY COLUMN INTO DATABASE
        for c in df_new_manual_flags.columns:
            dont_import = 0
            if c != 'TimeStamp':        
                print(c)

                # MERGE AUTOFLAG AND MANUAL FLAG DATA FOR COLUMN 
                df_merged = pd.concat([df_new_manual_flags[c].astype(int), df_auto_flags_pad[c + '_flagged'],df_auto_flags_pad['missing_data']], axis=1)
                df_merged.rename(columns={c: '_manual'}, inplace=True)
                df_merged.rename(columns={c + '_flagged': '_auto'}, inplace=True)
                
                # REMOVE ROWS WHERE DATA ARE MISSING
                df_flags_ret = df_merged[df_merged.missing_data == 0]
                del df_flags_ret['missing_data']
                df_flags_ret['sensorname'] = c # add sensor name to to allow iDB.write_flags to process
                
                # CHECK THERE ARE NO NAN's IN MANUAL FLAGS, IF SO STOP IMPORTATION 
                if df_flags_ret['flag_manual'].isnull().values.any():
                    print('nan\'s found - manual')
                    dont_import = 1

                # REPLACE ANY NaNs in AUTOFLAG DATA WITH -1 (then hopefully autoflagger will heal these)
                if df_flags_ret['flag_auto'].isnull().values.any() and dont_import == 0:
                    print('nan\'s found - auto')                
                    index = df_flags_ret['flag_auto'].index[df_flags_ret['flag_auto'].apply(np.isnan)]
                    for i in index:
                        #print(i)
                        #print(df_flags_ret.loc[i])
                        df_flags_ret.loc[i,'flag_auto'] = -1
                        #print(df_flags_ret.loc[i])

                info = {}
                info['colname'] = c
                info['site'] = site_name
                info['name'] = c

                # WRITE DATA TO DATABASE
                if dont_import == 0:
                    print('importing')

                    #c = iDB.influx_connection(db_info[0], db_info[1], db_info[2], db_info[3], customer)
                    #if c.make_connection():
                    #    iDB.inject_flags(info, df_flags_ret)
                else:
                    file_processed = 0 # so file does not get moved into processed folder
        
        file_processed += 1 

    except Exception as e:
        print(str(e))


    if file_processed == 3:
        # MOVE FILE TO PROCESSED DIRECTORY
        shutil.move(folder_processing + f, folder_processed + f)