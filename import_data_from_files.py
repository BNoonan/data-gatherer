
import os
import pandas as pd
import importlib
from datetime import datetime, timedelta


# ENERGY3 MODULES
import tools as tl
import interact_influxdb as iDB


def check_last_date(db_info, col_name, col_type, customer, site_name):
    """
    PURPOSE

    1. constructs query to find last timestamp of sensor in database, query limited to 1 value returned
    2. makes connnection to database and queries
    3. if there is data, grabs timestamp
    4. if no data, or the timestamp is to be ignored (overwritten data) returns 'None'

    """

    # EXTRACT DATA FROM MEASUREMENT
    what = 'value_adj, sensorname, type'
    where = ' where "sensorname" = \'{0}\' and "type" = \'{1}\''.format(col_name, col_type)
    when = ' order by time desc limit 1'
    flags = ''

    # MAKE DATABASE CONNECTION AND QUERY
    c = iDB.influx_connection(db_info[0], db_info[1], db_info[2], db_info[3], customer)
    if c.make_connection():
        query_result = c.query_database(what, site_name, flags, where, when)
        data = query_result['data']

    # IF THERE IS DATA THEN GRAB DATE
    if isinstance(data, pd.DataFrame):      
        last_date = data.index[0]
    else:
        last_date = 'None'

    # RETURN LAST DATE
    return last_date, c


def go_import_data(config_site, config_sys, overwrite_data, date_range):
    """
    PROCESS

    1. extracts site information from config
    2. if overwrite flag specified uses overwrite date range
    3. checks that each data file header is the same as that specified to ensure configuration that the datafile
       conflates to the site and that there have not been any configuration changes
    4. imports data from logger file(s)
    5. checks that the imported data falls into the date range specified by the configuration file (excluded if not)
    6. concatenates all data imported from the site
    7. removes duplicate rows
    8. removes any rows of missing data (ie all data are nan)
    9. injects data into influxdb

    :param config_site: site configuration information
    :param config_sys: configuration of system, see "system_information.json"
    :param overwrite_data: flag -> 1 overwrite existing data (or patch data gap), 0 append data.
    :param date_range: date range of data to be overwritten

    """



    # Extract site config folder
    site_config_folders = config_sys['site_config_folders']
    site_config_folder = site_config_folders[0]  # UPDATE - don't like non specific key

    # Extract database config
    db_info = [config_sys['host'],
               config_sys['port'],
               config_sys['user'],
               config_sys['password']]


    # EXTRACT SITE CONFIGURATION
    folder = config_site['file directory']
    customer = config_site['customer']
    site_name = config_site['site name']
    file_suffix = config_site['file name']
    logger_type = config_site['logger']
    header_file = config_site['file header']
    start_date = config_site['start date']
    end_date = config_site['end date']
    hours_adjust = config_site['hours adjust']



    # if data are to be overwritten, change date period specified in config file
    if overwrite_data == 1:
        start_date = date_range[0]
        end_date = date_range[1]   

    # START IMPORTING DATA FROM DATA DIRECTORY
    print()
    print('===========================================================================')
    print(customer + ': ' + site_name)
    print('===========================================================================')
    print()


    print('----------------------------------------------------------------------------------------')
    print('>> 1. Checking configuration file ' + folder)
    print('----------------------------------------------------------------------------------------')






    print('----------------------------------------------------------------------------------------')
    print('>> 2. Importing data from folder ' + folder)
    print('----------------------------------------------------------------------------------------')

    data_to_append = []  # list of data that is to be imported into the database
    not_imported_files = []  # list of files that cannot be imported

    # get list of data files in directory, sorted by name, open only those files with specified suffix
    file_list = os.listdir(folder)
    file_list.sort()
    files_opened_count = 0
    for file in file_list:
        if file.endswith(file_suffix):
            try:
                print('........ ' + file)
                print('         ........ checking file header against specified')

                # CHECK EACH FILE HEADER AGAINST SPECIFIED HEADER
                # Open header check file and stored header lines
                header_check_file = open(site_config_folder + header_file, 'r', encoding='ISO-8859-1')

                g = header_check_file.readlines()
                lines = []
                for d in g:
                    lines.append(d)

                # open data file and compare lines,
                data_file_header = open(folder + file, 'r', encoding='ISO-8859-1')

                match_or_not = True

                # run through header
                for e in range(0, len(g)):
                    f = data_file_header.readline()
                    
                    # compare lines, unless line to be ignored because of changeable field
                    if not lines[e].startswith('IGNORE'):                            
                        if f != lines[e]:
                            match_or_not = False
                            print('         ........ *****************************************************************')
                            print('                  ' + f)
                            print('         ........ -----------------------------------------------------------------')
                            print('                  ' + lines[e])
                            print('         ........ *****************************************************************')
               
                # if file header does not match don't import data
                if not match_or_not:
                    print('........ ........ file header does not match')
                    not_imported_files.append(file)

                # if file header matches, import data after checking dates
                if match_or_not:
                    # import data importation module and get data
                    data_parser = importlib.import_module('import_' + logger_type, package='none')
                    imported_data = data_parser.import_data(folder, file)               

                    # CHECK DATES IN FILE FALL IN SPECIFIED RANGE, AND GRAB ANY DATA THAT DOES
                    imported_data = imported_data[(imported_data.index >= start_date)
                                                  & (imported_data.index <= end_date)]

                    if len(imported_data) > 0:
                        print('         ........ importing')

                        # CONVERT TIME TO LOCAL AS SPECIFIED IN SITE CONFIG FILE
                        imported_data = imported_data.shift(hours_adjust, freq='H')
                        data_to_append.append(imported_data)
                        files_opened_count += 1
                    else:
                        print('         ........ data not in range of config file')

            except Exception as e:
                print(str(e))
                # builds list of files it can't import for some reason
                not_imported_files.append(file)

    print()
    print()
    print('........ number of files opened = ' + str(files_opened_count))
    print()

    # prints list of files that could not be imported 
    if len(not_imported_files) > 0:
        print()
        print()
        print('........ files that could not be imported')
        for f in not_imported_files:
            print('         ........' + f)


    # if there are data to append
    if len(data_to_append) == 0:
        print()
        print('........ no data to append')
        print()

    # APPEND DATA
    else:

        print()
        print('----------------------------------------------------------------------------------------')
        print('>> 3. Concatenating data and removing duplicate rows')
        print('----------------------------------------------------------------------------------------')
        # CONCATENATE ALL IMPORTED DATA
        imported_data = pd.concat(data_to_append, axis=0)
        print()
        print('........ data concatenated')
        print()

        # CHECK FOR DUPLICATE TIMESTAMPS AND FIX, SORT DATA BY TIME IF NEEDED (UPDATE???) # TODO - check
        imported_data = tl.check_duplicate_timestamps(imported_data, [imported_data.index[0], imported_data.index[-1]])
        imported_data = imported_data.sort_index()

        # LIMIT DATA TO OVERWRITE PERIOD IF WANTED
        if overwrite_data == 1:
        	imported_data = imported_data[start_date : end_date]

        # DROP ANY ROW WHERE ALL COLUMNS ARE NAN as these data are missing (excluding timestamp)
        imported_data.dropna(how='all', inplace=True) 


        # INJECT DATA
        print()
        print()
        print()
        print('----------------------------------------------------------------------------------------')
        print('>> 4. Injecting data and flags into database')   
        print('----------------------------------------------------------------------------------------')         


        # GET LIST OF COLUMN NAMES FROM SITE CONFIG FILE
        column_names = config_site['column names']
        info = {}
        for col in column_names:
            print()
            print('........ ' + col)

            # INJECT MAIN SENSOR DATA INTO DATABASE            
            info['site'] = site_name
            info['colname'] = col
            info['name'] = config_site['column names'][col]['name']
            info['type'] = config_site['column names'][col]['type']
            info['offset'] = config_site['column names'][col]['offset']
            info['param'] = config_site['column names'][col]['parameter']

            # CHECK FOR LAST TIMESTAMP OF SENSOR IN DATABASE
            print()
            print('         ........ checking for last entry')
            last_date, c = check_last_date(db_info, info['name'], info['type'], customer, site_name)
            print('         LAST DATE RECORDED IN DATABASE IS : ' + str(last_date))
            print()

            # if no date in data base, or data to be overwritten
            if last_date == 'None':       	
                last_date = imported_data.index[0]                  

            if overwrite_data == 1:
                last_date = imported_data.index[0] 

            # MAKE DATABASE CONNECTION AND INJECT DATA
            print()
            print('         ........ injecting data') 

            # declare dataframe in which to store data
            data_df = pd.DataFrame(index=imported_data.index)

            # create raw values column
            data_df[col + '_raw'] = round(imported_data[col],2)

            # create adj values column and add offset if required
            data_df[col + '_adj'] = round(imported_data[col] + info['offset'],2)

            # correct vane data where offset makes more than 360 degrees
            if info['param'] == 'wind direction':
                # find where data > 360 deg
                mask = data_df[col + '_adj'] > 360
                # subtract 360 and round
                data_df[col + '_adj'][mask] = round(data_df[col + '_adj'][mask] - 360, 2)

            print('NULL VALUES DETECTED : ' + str(data_df.isnull().values.any()))


            if c.make_connection():
                inject_result = c.inject_data(info , data_df.loc[last_date:imported_data.index[-1]])
                print(inject_result)





            # INJECT FLAGS INTO DATABASE               
            print()
            print('         ........ injecting flags')                    
            
            # declare dataframe in which to store flags
            flags_df = pd.DataFrame(index=imported_data.index)
            
            # If there are manual flags in imported data, copy
            if col + '_flagged' in imported_data.columns:
                flags_df[col + '_manual'] = imported_data[col + '_flagged']

            # If not make manual flags all -1 using data as template
            else:
                flags_df[col + '_manual'] = -1

            # create auto flag column as all -1's for autoflagger
            flags_df[col + '_auto'] = -1

            print(flags_df)
            print('NULL VALUES DETECTED : ' + str(flags_df.isnull().values.any()))

            # MAKE DATABASE CONNECTION AND INJECT FLAGS           
            if c.make_connection():
                inject_result = c.inject_flags(info, flags_df.loc[last_date:imported_data.index[-1]])
                print(inject_result)






            # INJECT ASSOCIATED COLUMN DATA INTO DATABASE               
            print()
            print()
            associated_column_names = config_site['column names'][col]['associated columns']
            if len(associated_column_names) == 0:
                print('         ........ no associate data in config file')

            for assoc in associated_column_names: 
                print()
                print('........ ' + assoc)
                
                info['colname'] = assoc
                info['type'] = config_site['column names'][col]['associated columns'][assoc]
                info['offset'] = ''            

                # CHECK FOR LAST TIMESTAMP OF SENSOR (ASSOC) IN DATABASE
                print()
                print('        ........ checking for last entry')
                last_date, c = check_last_date(db_info, info['name'], info['type'], customer, site_name)
                print('         LAST DATE RECORDED IN DATABASE IS : ' + str(last_date))
                print()

                # if no date in data base, or data to be overwritten
                if last_date == 'None':             
                    last_date = imported_data.index[0]

                if overwrite_data == 1:
                    last_date = imported_data.index[0]

                # MAKE DATABASE CONNECTION AND INJECT                
                print()
                print('         ........ injecting associate data')

                # declare dataframe in which to store data
                data_df = pd.DataFrame(index=imported_data.index)
                # create raw values column
                data_df[assoc + '_raw'] = round(imported_data[col],2)
                # create adj values column and add offset if required
                data_df[assoc + '_adj'] = round(imported_data[col],2)            

                print(data_df)
                print('NULL VALUES DETECTED : ' + str(data_df.isnull().values.any()))

                if c.make_connection():
                    inject_result = c.inject_data(info,data_df.loc[last_date:imported_data.index[-1]])
                    print(inject_result)


            print()
            print()

